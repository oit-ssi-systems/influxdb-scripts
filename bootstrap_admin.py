#!/usr/bin/env python3
import sys
import argparse
from influxdb import InfluxDBClient
import os
import hvac


def parse_args():
    parser = argparse.ArgumentParser(
        description='Setup permissions on an influxdb host')
    parser.add_argument('target_server', help='Target InfluxDB server')

    return parser.parse_args()


def main():

    args = parse_args()

    vault = hvac.Client(url=os.environ['VAULT_ADDR'],
                        token=os.environ['VAULT_TOKEN'])

    admin_creds = vault.read(
        'secret/linux/influxdb/%s/admin' % args.target_server)

    nonauth_client = InfluxDBClient(args.target_server, ssl=True,
                                    verify_ssl=True)

    auth_client = InfluxDBClient(args.target_server,
                                 username=admin_creds['data']['username'],
                                 password=admin_creds['data']['password'],
                                 ssl=True,
                                 verify_ssl=True
                                 )
    try:
        # See if we are connected
        nonauth_client.query('show users')
    except Exception as e:
        sys.stderr.write((
            "Could not connect unauthenticated.  Disable auth for admin "
            "user creation {}\n".format(e)
        ))
        sys.exit(2)
    print("Creating admin user")
    nonauth_client.create_user(
        username=admin_creds['data']['username'],
        password=admin_creds['data']['password'],
        admin=True
    )
    return 0


if __name__ == "__main__":
    sys.exit(main())
