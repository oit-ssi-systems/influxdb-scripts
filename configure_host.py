#!/usr/bin/env python3
import sys
import argparse
from influxdb import InfluxDBClient
import os
import hvac


def parse_args():
    parser = argparse.ArgumentParser(
        description='Setup permissions on an influxdb host')
    parser.add_argument('target_server', help='Target InfluxDB server')

    return parser.parse_args()


def get_or_create_user(influx_client, username, password):
    """Get a user object if it exists, or create it
    """
    existing_users = []
    for item in influx_client.get_list_users():
        existing_users.append(item['user'])

    if username not in existing_users:
        print("Creating user %s" % username)
        influx_client.create_user(
            username=username,
            password=password
        )
    else:
        print("Setting password for %s" % username)
        influx_client.set_user_password(
            username=username,
            password=password
        )


def main():

    args = parse_args()

    vault = hvac.Client(url=os.environ['VAULT_ADDR'],
                        token=os.environ['VAULT_TOKEN'])

    admin_creds = vault.read(
        'secret/linux/influxdb/%s/admin' % args.target_server)

    auth_client = InfluxDBClient(args.target_server,
                                 username=admin_creds['data']['username'],
                                 password=admin_creds['data']['password'],
                                 ssl=True,
                                 verify_ssl=True
                                 )
    # Set up global users first
    ignore_users = ['admin']
    existing_users = []
    for item in auth_client.get_list_users():
        existing_users.append(item['user'])
    for vault_user in vault.list(
            'secret/linux/influxdb/%s/' % args.target_server)['data']['keys']:
        # Skip certain users, like admin
        if vault_user in ignore_users:
            continue
        vault_user_info = vault.read(
            'secret/linux/influxdb/%s/%s' % (args.target_server, vault_user)
        )
        if vault_user not in existing_users:
            print("Creating %s" % vault_user)
            auth_client.create_user(
                username=vault_user_info['data']['username'],
                password=vault_user_info['data']['password']
            )
        else:
            print("Setting password for %s" % vault_user)
            auth_client.set_user_password(
                username=vault_user_info['data']['username'],
                password=vault_user_info['data']['password']
            )

    # Existing databases
    existing_databases = []
    for item in auth_client.get_list_database():
        existing_databases.append(item['name'])

    # Create and configure database listed in Vault
    for item in vault.list(
            'secret/linux/influxdb/databases')['data']['keys']:
        db_name = item[:-1]

        if db_name not in existing_databases:
            print("Creating database {}".format(db_name))
            auth_client.create_database(db_name)
            existing_databases.append(db_name)

        permission_mapping = {
            'rw_users': 'all',
            'wo_users': 'write',
            'ro_users': 'read'
        }
        for permission_title, permission in permission_mapping.items():

            users_raw = vault.list(
                'secret/linux/influxdb/databases/{}/{}'.format(
                    db_name, permission_title))

            if users_raw:
                users = users_raw['data']['keys']

                for user in users:
                    auth_info = vault.read(
                        'secret/linux/influxdb/databases/{}/{}/{}'.format(
                            db_name, permission_title, user
                        )
                    )['data']

                    get_or_create_user(auth_client, auth_info['username'],
                                       auth_info['password'])

                    print("Granting {} on {} to {}".format(
                        permission, db_name, auth_info['username']
                    ))
                    auth_client.grant_privilege(permission,
                                                db_name,
                                                auth_info['username'])

    # Now apply global perms
    policies = {
        '10years': {
            'duration': '87600h',
            'shard_duration': '7d',
            'default': True
        }
    }

    for database in existing_databases:
        print("Granting default rights for: %s" % database)
        auth_client.grant_privilege('read', database, 'readonly')
        auth_client.grant_privilege('write', database, 'telegraf')

        # Do retention policy setup
        existing_policy_names = [
            x['name'] for x in
            auth_client.get_list_retention_policies(database)
        ]
        # Create default policy
        for policy_name, policy_values in policies.items():
            if policy_name not in existing_policy_names:
                print("Creating {} policy on {}".format(policy_name, database))
                auth_client.create_retention_policy(
                    name=policy_name,
                    duration=policy_values['duration'],
                    replication=1,
                    database=database,
                    shard_duration=policy_values['shard_duration'],
                    default=policy_values['default']
                )

        # Remove autogen policy if it exists
        if 'autogen' in existing_policy_names:
            print("Dropping autogen policy from {}".format(database))
            auth_client.drop_retention_policy('autogen', database=database)

    return 0


if __name__ == "__main__":
    sys.exit(main())
