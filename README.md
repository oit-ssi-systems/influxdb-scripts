# InfluxDB Configuration

## Overview

Puppet will install and do base configuration on InfluxDB.  These scripts will
reach in to InfluxDB and set up the base users, DB's, etc

## Bootstrapping

The bootstrapping script will set up the initial admin user.  To do this, make
sure that authentication is disabled in /etc/influxdb/influxdb.conf:

```
[http]
  auth-enabled = false
```

Ensure you have an admin username and password set up in Vault under the path:

```
 $ vault read secret/linux/influxdb/${INFLUX_HOSTNAME}.oit.duke.edu/admin
 Key                 Value
 ---                 -----
 refresh_interval    24h
 password            redacted
 username            admin
```

Run the bootstrap script with:

```
$ ./bootstrap_admin.py ${INFLUX_HOSTNAME}-dev-01.oit.duke.edu
```

Once the admin user is set, go back in to the influxdb.conf file and re-enable
authentication

## Configuration

Once bootstrapped, the configuration script will create the various users and DBs specified in vault

These are shared between all instances of InfluxDB.  They should be listed like such in Vault:

```
 $ vault read secret/linux/influxdb/databases/${DATABASE_NAME}/${PERMISSION_SCHEME}/${USERNAME}
 Key                 Value
 ---                 -----
 refresh_interval    24h
 password            bar
 username            test
```

Possibly permission schemes are defined in the script as:

```python
    permission_mapping = {
        'rw_users': 'all',
        'wo_users': 'write',
        'ro_users': 'read'
    }
```
